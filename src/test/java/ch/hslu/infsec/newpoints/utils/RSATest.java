package ch.hslu.infsec.newpoints.utils;

import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.security.SecureRandom;

import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class RSATest {

    private RSA rsa;
    private final SecureRandom secureRandom;

    public RSATest() {
        this.secureRandom = new SecureRandom();
    }

    @Before
    public void setUp() {
        rsa = new RSA(secureRandom);
    }


    @Test
    public void testSign() {
        AsymmetricCipherKeyPair keyPair = rsa.generateKeyPair(1024);

        byte[] message = {0x00, 0x01, 0x02};

        byte[] signature = rsa.sign(keyPair.getPrivate(), message);

        assertTrue(rsa.verify(keyPair.getPublic(), message, signature));
    }




}
