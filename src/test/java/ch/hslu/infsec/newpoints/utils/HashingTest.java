package ch.hslu.infsec.newpoints.utils;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class HashingTest {

    Hashing hashing;

    @Before
    public void setUp() throws Exception {
        this.hashing = new Hashing();
    }

    @Test
    public void testHashMessage() throws Exception {
        byte[] message = {0x00, 0x01, 0x02};

        byte[] hashedmessage = hashing.hashMessage(message);

        assertFalse(Arrays.equals(message, hashedmessage));
        assertTrue(Arrays.equals(hashedmessage, hashing.hashMessage(message)));
    }
}