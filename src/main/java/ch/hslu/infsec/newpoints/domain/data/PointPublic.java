package ch.hslu.infsec.newpoints.domain.data;

public class PointPublic {

    private byte[] hashedBlindedId;
    private byte[] signature;


    public byte[] getHashedBlindedId() {
        return hashedBlindedId;
    }

    public void setHashedBlindedId(byte[] hashedBlindedId) {
        this.hashedBlindedId = hashedBlindedId;
    }

    public byte[] getSignature() {
        return signature;
    }

    public void setSignature(byte[] signature) {
        this.signature = signature;
    }



}
