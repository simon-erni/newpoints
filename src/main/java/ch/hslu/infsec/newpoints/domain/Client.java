package ch.hslu.infsec.newpoints.domain;

import ch.hslu.infsec.newpoints.domain.data.PointPrivate;
import ch.hslu.infsec.newpoints.domain.data.PointPublic;
import ch.hslu.infsec.newpoints.utils.Blinder;
import ch.hslu.infsec.newpoints.utils.Hashing;
import ch.hslu.infsec.newpoints.utils.RSA;
import com.google.inject.Inject;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Client {

    private final RSA rsa;
    private final SecureRandom secureRandom;
    private final Hashing hashing;
    private final Blinder blinder;

    private final LinkedList<AsymmetricCipherKeyPair> keys;
    private final LinkedList<PointPrivate> pointPrivates;


    @Inject
    public Client(RSA rsa, SecureRandom secureRandom, Hashing hashing, Blinder blinder) {
        this.rsa = rsa;
        this.secureRandom = secureRandom;
        this.hashing = hashing;
        this.blinder = blinder;
        keys = new LinkedList<AsymmetricCipherKeyPair>();
        pointPrivates = new LinkedList<PointPrivate>();
    }

    public AsymmetricKeyParameter getFreshPublicKey() {
        AsymmetricCipherKeyPair keyPair = rsa.generateKeyPair(1024);

        keys.add(keyPair);

        return keyPair.getPublic();
    }

    public List<PointPublic> generatePoints(int count, AsymmetricKeyParameter clientPublicKey, AsymmetricKeyParameter vendorPublicKey) {

        AsymmetricCipherKeyPair localKeyPair = getCorrespondingKeyPair(clientPublicKey);

        List<PointPublic> generatedPoints = new LinkedList<PointPublic>();


        for (int i = 0; i < count; i++) {


            byte[] randomPointId = generateRandomId();

            byte[] hashedPointId = hashing.hashMessage(randomPointId);

            BigInteger blindingFactor = blinder.generateBlindingFactor(vendorPublicKey);


            byte[] blindedHashedId = blinder.blind(vendorPublicKey, blindingFactor, hashedPointId);

            byte[] signature = rsa.sign(localKeyPair.getPrivate(), blindedHashedId);


            PointPublic pointPublic = new PointPublic();
            pointPublic.setHashedBlindedId(blindedHashedId);
            pointPublic.setSignature(signature);

            generatedPoints.add(pointPublic);

            savePointPrivate(randomPointId, blindedHashedId, blindingFactor, vendorPublicKey);
        }


        return generatedPoints;
    }

    public void verifySignedPoints(List<PointPublic> signedPointPublics) {
        for (PointPublic signedPointPublic : signedPointPublics) {

            PointPrivate pointPrivate = findCorrespondingPoint(signedPointPublic);


            if (!rsa.verify(pointPrivate.getVendorPublicKey(), signedPointPublic.getHashedBlindedId(), signedPointPublic.getSignature())) {
                throw new AssertionError("Server did not sign our key");
            }

            pointPrivate.setServerSignature(signedPointPublic.getSignature());
        }

        System.out.println("Verified all coins");
    }

    private PointPrivate findCorrespondingPoint(PointPublic pointPublic) {

        for (PointPrivate pointPrivate : pointPrivates) {
            if (Arrays.equals(pointPrivate.getRandomHashedBlindedId(), pointPublic.getHashedBlindedId())) {
                return pointPrivate;
            }
        }

        throw new AssertionError("could not find local corresponding generated point");
    }

    private void savePointPrivate(byte[] randomPointId, byte[] hashedPointId, BigInteger blindingFactor, AsymmetricKeyParameter vendorPublicKey) {
        PointPrivate pointPrivate = new PointPrivate();


        pointPrivate.setRandomId(randomPointId);
        pointPrivate.setBlindingFactor(blindingFactor);
        pointPrivate.setVendorPublicKey(vendorPublicKey);
        pointPrivate.setRandomHashedBlindedId(hashedPointId);

        pointPrivates.add(pointPrivate);
    }


    private byte[] generateRandomId() {
        byte[] pointRandomNumber = new byte[1024];
        secureRandom.nextBytes(pointRandomNumber);

        return pointRandomNumber;
    }

    private AsymmetricCipherKeyPair getCorrespondingKeyPair(AsymmetricKeyParameter publicKey) {

        for (AsymmetricCipherKeyPair key : keys) {
            if (key.getPublic().equals(publicKey)) {
                return key;
            }
        }

        throw new AssertionError("Keypair not found locally");

    }


}
