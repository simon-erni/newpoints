package ch.hslu.infsec.newpoints.domain;

import ch.hslu.infsec.newpoints.domain.data.PointPublic;
import ch.hslu.infsec.newpoints.utils.RSA;
import com.google.inject.Inject;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;

import java.util.LinkedList;
import java.util.List;

public class Vendor {

    private final RSA rsa;
    private final AsymmetricCipherKeyPair keyPair;

    private AsymmetricKeyParameter publicKeyClient;

    //Subject to timing account. Link to Public Key Client. Maybe generate transaction Id. PoC.
    private int pointsAmount;

    @Inject
    public Vendor(RSA rsa) {
        this.rsa = rsa;
        keyPair = rsa.generateKeyPair(1024);

    }

    public void initTransaction(AsymmetricKeyParameter publicKeyClient, int pointsAmount) {
        this.publicKeyClient = publicKeyClient;
        this.pointsAmount = pointsAmount;
    }


    public AsymmetricKeyParameter getPublicKey() {
        return keyPair.getPublic();
    }

    public List<PointPublic> signPoints(List<PointPublic> clientPoints) {

        if (clientPoints.size() != pointsAmount) {
            throw new AssertionError("Supplied Points not the same as requested");
        }

        List<PointPublic> signedPoints = new LinkedList<PointPublic>();


        for (PointPublic clientPoint : clientPoints){

            if (!rsa.verify(publicKeyClient, clientPoint.getHashedBlindedId(), clientPoint.getSignature())) {
                throw new AssertionError("Not valid Signature on Point Request");
            }

            System.out.println("verified a point on the server");

            PointPublic signedPointPublic = new PointPublic();

            signedPointPublic.setHashedBlindedId(clientPoint.getHashedBlindedId());
            signedPointPublic.setSignature(rsa.sign(keyPair.getPrivate(), clientPoint.getHashedBlindedId()));

            signedPoints.add(signedPointPublic);
        }

        return signedPoints;
    }


}
