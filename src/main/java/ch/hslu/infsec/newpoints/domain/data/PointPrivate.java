package ch.hslu.infsec.newpoints.domain.data;

import org.bouncycastle.crypto.params.AsymmetricKeyParameter;

import java.math.BigInteger;

public class PointPrivate {

    private byte[] randomId;
    private BigInteger blindingFactor;
    private byte[] randomHashedBlindedId;
    private AsymmetricKeyParameter vendorPublicKey;
    private byte[] serverSignature;

    public BigInteger getBlindingFactor() {
        return blindingFactor;
    }

    public void setBlindingFactor(BigInteger blindingFactor) {
        this.blindingFactor = blindingFactor;
    }

    public byte[] getRandomId() {
        return randomId;
    }

    public void setRandomId(byte[] randomId) {
        this.randomId = randomId;
    }

    public byte[] getRandomHashedBlindedId() {
        return randomHashedBlindedId;
    }

    public void setRandomHashedBlindedId(byte[] randomHashedBlindedId) {
        this.randomHashedBlindedId = randomHashedBlindedId;
    }

    public AsymmetricKeyParameter getVendorPublicKey() {
        return vendorPublicKey;
    }

    public void setVendorPublicKey(AsymmetricKeyParameter vendorPublicKey) {
        this.vendorPublicKey = vendorPublicKey;
    }


    public byte[] getServerSignature() {
        return serverSignature;
    }

    public void setServerSignature(byte[] serverSignature) {
        this.serverSignature = serverSignature;
    }
}
