package ch.hslu.infsec.newpoints;

import ch.hslu.infsec.newpoints.domain.Client;
import ch.hslu.infsec.newpoints.domain.data.PointPublic;
import ch.hslu.infsec.newpoints.domain.Vendor;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;

import java.util.List;

public class Application {

    private final Client client;
    private final Vendor vendor;

    @Inject
    public Application(Client client, Vendor vendor) {
        this.client = client;
        this.vendor = vendor;
    }


    public void main() {

        AsymmetricKeyParameter vendorPublicKey = vendor.getPublicKey();
        System.out.println("got vendor public key");

        AsymmetricKeyParameter clientPublicKey = client.getFreshPublicKey();
        vendor.initTransaction(clientPublicKey, 1);

        List<PointPublic> generatedPoints = client.generatePoints(1, clientPublicKey, vendorPublicKey);

        List<PointPublic> signedPoints = vendor.signPoints(generatedPoints);

        client.verifySignedPoints(signedPoints);


    }

    public static void main(String[] args) throws Exception {

        Injector injector = Guice.createInjector();

        Application app = injector.getInstance(Application.class);
        app.main();

    }
}
