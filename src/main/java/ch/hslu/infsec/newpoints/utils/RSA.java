package ch.hslu.infsec.newpoints.utils;

import com.google.inject.Inject;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.Signer;
import org.bouncycastle.crypto.digests.SHA1Digest;
import org.bouncycastle.crypto.engines.RSAEngine;
import org.bouncycastle.crypto.generators.RSAKeyPairGenerator;
import org.bouncycastle.crypto.params.RSAKeyGenerationParameters;
import org.bouncycastle.crypto.signers.PSSSigner;

import java.math.BigInteger;
import java.security.SecureRandom;

public class RSA {

    private final SecureRandom secureRandom;

    @Inject
    public RSA(SecureRandom secureRandom){
        this.secureRandom = secureRandom;
    }

    public AsymmetricCipherKeyPair generateKeyPair(int keySize) {
        RSAKeyPairGenerator r = new RSAKeyPairGenerator();

        r.init(new RSAKeyGenerationParameters(new BigInteger("10001", 16), secureRandom,
                keySize, 80));

        return r.generateKeyPair();
    }


    public byte[] sign(CipherParameters key, byte[] toSign) {
        SHA1Digest dig = new SHA1Digest();
        RSAEngine eng = new RSAEngine();

        Signer signer = new PSSSigner(eng, dig, 15);
        signer.init(true, key);
        signer.update(toSign, 0, toSign.length);

        byte[] sig = null;

        try {
            sig = signer.generateSignature();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return sig;
    }


    public byte[] signBlinded(CipherParameters key, byte[] msg) {
        RSAEngine signer = new RSAEngine();
        signer.init(true, key);
        return signer.processBlock(msg, 0, msg.length);
    }


    public boolean verify(CipherParameters key, byte[] msg, byte[] sig) {
        PSSSigner signer = new PSSSigner(new RSAEngine(), new SHA1Digest(), 15);
        signer.init(false, key);

        signer.update(msg,0,msg.length);

        return signer.verifySignature(sig);
    }



}
