package ch.hslu.infsec.newpoints.utils;

import java.security.MessageDigest;

public class Hashing {

    private final MessageDigest messageDigest;

    public Hashing() throws Exception {
        this.messageDigest = MessageDigest.getInstance("SHA-256");
    }

    public synchronized byte[] hashMessage(byte[] message) {
        messageDigest.update(message);

        return messageDigest.digest();
    }
}
