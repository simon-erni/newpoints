package ch.hslu.infsec.newpoints.utils;

import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.digests.SHA1Digest;
import org.bouncycastle.crypto.engines.RSABlindingEngine;
import org.bouncycastle.crypto.generators.RSABlindingFactorGenerator;
import org.bouncycastle.crypto.params.RSABlindingParameters;
import org.bouncycastle.crypto.params.RSAKeyParameters;
import org.bouncycastle.crypto.signers.PSSSigner;

import java.math.BigInteger;

public class Blinder {


    public BigInteger generateBlindingFactor(CipherParameters pubKey) {
        RSABlindingFactorGenerator gen = new RSABlindingFactorGenerator();

        gen.init(pubKey);

        return gen.generateBlindingFactor();
    }


    public byte[] blind(CipherParameters key, BigInteger factor, byte[] msg) {
        RSABlindingEngine eng = new RSABlindingEngine();

        RSABlindingParameters params = new RSABlindingParameters((RSAKeyParameters) key, factor);
        PSSSigner blindSigner = new PSSSigner(eng, new SHA1Digest(), 15);
        blindSigner.init(true, params);

        blindSigner.update(msg, 0, msg.length);

        byte[] blinded = null;
        try {
            blinded = blindSigner.generateSignature();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return blinded;
    }


    public byte[] unblind(CipherParameters key, BigInteger factor, byte[] msg) {
        RSABlindingEngine eng = new RSABlindingEngine();

        RSABlindingParameters params = new RSABlindingParameters((RSAKeyParameters) key, factor);
        eng.init(false, params);

        return eng.processBlock(msg, 0, msg.length);
    }

}
