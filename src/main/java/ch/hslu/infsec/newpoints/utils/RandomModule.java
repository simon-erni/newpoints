package ch.hslu.infsec.newpoints.utils;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;

import java.security.SecureRandom;

public class RandomModule extends AbstractModule{

    @Provides
    @Singleton
    SecureRandom getSecureRandom() {
        return new SecureRandom();
    }

    @Override
    protected void configure() {

    }
}
